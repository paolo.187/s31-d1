// Contains all task endpoints for our applications

const express = require('express');
const router = express.Router();

const taskController = require('../controllers/taskControllers')

router.get('/',(req,res)=>{

	taskController.getAllTasks().then(resultFromController => res.send(resultFromController))
})

module.exports = router


router.post('/createTask',(req,res)=>  {
	taskController.createTask(req.body).then(resultFromController=> res.send(resultFromController))
})


//route for deleting task

router.delete('/deleteTask/:id', (req,res)=>{

	taskController.deleteTask(req.params.id).then(resultFromController => res.send(resultFromController))
})
	
//route to update

router.put('/updateTask/:id', (req,res)=> {

	taskController.updateTask(req.params.id,req.body).then(resultFromController=>res.send(resultFromController))
})


////Activity

	//Route for specific task

router.get('/taskGet/:id',(req,res)=>{

	taskController.getOneTask(req.params.id).then(resultFromController => res.send(resultFromController))
})


	//Update to Completed Task


router.put('/:id/completed', (req,res)=> {

	taskController.completedTask(req.params.id,req.body).then(resultFromController=>res.send(resultFromController))
})