
const mongoose = require('mongoose');

const taskSchema = new mongoose.Schema({

		name: {
			type:String,
			required: [true, 'Task Name is Required']
		},

		status: {
			type: String,
			default: "pending"
		}
});
//get all tasks
module.exports = mongoose.model('Task', taskSchema)

//creating new task

